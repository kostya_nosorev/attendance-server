﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using attendance_server.DbContexts;
using attendance_server.Infrastructure;
using attendance_server.Interfaces;
using attendance_server.Models;
using attendance_server.Views;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace attendance_server.Controllers
{
    public class AccountController : Controller
    {
        private IAllUsers Users;

        public AccountController(IAllUsers allUsers)
        {
            Users = allUsers;
        }

        [HttpPost("/token")]
        public IActionResult Token([FromBody] AuthModel model)
        {
            var identity = GetIdentity(model.username, model.password);
            if (identity == null)
            {
                return BadRequest(new {errorText = "Invalid username or password."});
            }

            var now = DateTime.UtcNow;
            // создаем JWT-токен
            var jwt = new JwtSecurityToken(
                issuer: AuthOptions.ISSUER,
                audience: AuthOptions.AUDIENCE,
                notBefore: now,
                claims: identity.Claims,
                expires: now.Add(TimeSpan.FromMinutes(AuthOptions.LIFETIME)),
                signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(),
                    SecurityAlgorithms.HmacSha256));
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            var response = new
            {
                access_token = encodedJwt,
                username = identity.Name
            };

            return Json(response);
        }

        private ClaimsIdentity GetIdentity(string login, string password)
        {
            var user = Users.GetUserByLoginPassword(login, password);
            if (user == null)
                return null;

            var claims = new List<Claim>
            {
                new Claim("Username", user.Login),
                new Claim("ID", user.Id.ToString()),
                new Claim("Role", user.UserType.ToString())
            };
            var claimsIdentity = new ClaimsIdentity(claims, "Token", "Username",
                "Role");
            return claimsIdentity;
        }
    }
}