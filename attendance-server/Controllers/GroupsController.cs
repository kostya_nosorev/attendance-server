﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using attendance_server.Interfaces;
using attendance_server.Models;
using attendance_server.Models.Controllers;
using attendance_server.Views;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;

namespace attendance_server.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class GroupsController : ControllerBase
    {
        private readonly IAllGroups allGroups;
        private readonly IMapper mapper;

        public GroupsController(IAllGroups allGroups, IMapper mapper)
        {
            this.allGroups = allGroups;
            this.mapper = mapper;
        }

        [HttpGet]
        public IEnumerable<GroupsViewModel> List()
        {
            return mapper.Map<List<GroupsViewModel>>(allGroups.GetAll());
        }

        [HttpGet]
        [Route("{id}")]
        public ActionResult<GroupsViewModel> Get(int id)
        {
            var group = allGroups.GetById(id);
            if (group == null)
                return NotFound();
            return mapper.Map<GroupsViewModel>(group);
        }

        [HttpPost]
        [Route("add")]
        // [Authorize]
        public async Task<ActionResult<Group>> Post([FromBody] AddGroup addGroup)
        {
            // if (User.Identity is not ClaimsIdentity identifier) return null;
            // if (identifier.FindFirst("Role").Value != "Teacher") return Forbid();

            Console.WriteLine($"Post group");
            var groupObj = mapper.Map<Group>(addGroup);
            allGroups.Add(groupObj);
            return Ok();
        }


        [HttpPost]
        [Route("addStudents")]
        // [Authorize]
        public async Task<ActionResult<Group>> Post([FromBody] AddStudentsToGroup objects)
        {
            // if (User.Identity is not ClaimsIdentity identifier) return null;
            // if (identifier.FindFirst("Role").Value != "Teacher") return Forbid();

            allGroups.AddStudentsToGroupByStudentId(objects.GroupId, objects.StudentsIds);
            return Ok();
        }
    }
}