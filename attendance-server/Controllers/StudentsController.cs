﻿using System.Collections.Generic;
using System.Threading.Tasks;
using attendance_server.Interfaces;
using attendance_server.Models;
using attendance_server.Models.Controllers;
using attendance_server.Views;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;

namespace attendance_server.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Produces("application/json")]
    public class StudentsController : ControllerBase
    {
        private readonly IAllItems<Student> allStudents;
        private readonly IMapper mapper;

        public StudentsController(IAllItems<Student> allStudents, IMapper mapper)
        {
            this.allStudents = allStudents;
            this.mapper = mapper;
        }

        [HttpGet]
        public IEnumerable<StudentsViewModel> List()
        {
            var students = allStudents.GetAll();

            return mapper.Map<List<StudentsViewModel>>(students);
        }

        [HttpGet]
        [Route("{id}")]
        public ActionResult<StudentsViewModel> Get(int id)
        {
            var student = allStudents.GetById(id);
            if (student == null)
                return NotFound();
            return mapper.Map<StudentsViewModel>(student);
        }


        [HttpPost]
        [Route("add")]
        // [Authorize]
        public StudentsViewModel Post([FromBody] AddStudent body)
        {
            // if (User.Identity is not ClaimsIdentity identifier) return null;
            // if (identifier.FindFirst("Role").Value != "Teacher") return Forbid();
            // var student = new Student {UserId = body.userId, BirthDate = body.BirthDate, UUID = body.UUID};
            var student = mapper.Map<Student>(body);
            allStudents.Add(student);

            return mapper.Map<StudentsViewModel>(student);
        }


        [HttpDelete]
        [Route("{id}")]
        // [Authorize]
        public async Task<ActionResult<int>> Delete(int id)
        {
            // if (User.Identity is not ClaimsIdentity identifier) return null;
            // if (identifier.FindFirst("Role").Value != "Teacher") return Forbid();

            return Ok(allStudents.DeleteById(id));
        }
    }
}