﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using attendance_server.DbContexts;
using attendance_server.Interfaces;
using attendance_server.Mocks;
using attendance_server.Models;
using attendance_server.Models.Controllers;
using attendance_server.Views;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace attendance_server.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Produces("application/json")]
    public class TeachersController : ControllerBase
    {
        private readonly IAllItems<Teacher> allTeachers;
        private readonly IMapper mapper;

        public TeachersController(IAllItems<Teacher> allTeachers, IMapper mapper)
        {
            this.allTeachers = allTeachers;
            this.mapper = mapper;
        }

        [HttpGet]
        public IEnumerable<TeachersViewModel> List()
        {
            var students = allTeachers.GetAll();

            return mapper.Map<List<TeachersViewModel>>(students);
        }

        [HttpGet]
        [Route("{id}")]
        public ActionResult<TeachersViewModel> Get(int id)
        {
            var teacher = allTeachers.GetById(id);
            if (teacher == null)
                return NotFound();
            return mapper.Map<TeachersViewModel>(teacher);
        }


        [HttpPost]
        [Route("add")]
        // [Authorize]
        public TeachersViewModel Post([FromBody] AddTeacher body)
        {
            // if (User.Identity is not ClaimsIdentity identifier) return null;
            // if (identifier.FindFirst("Role").Value != "Teacher") return Forbid();
            var teacher = mapper.Map<Teacher>(body);
            allTeachers.Add(teacher);

            return mapper.Map<TeachersViewModel>(teacher);
        }


        [HttpDelete]
        [Route("{delete}")]
        // [Authorize]
        public async Task<ActionResult<int>> DeleteTeacher([FromBody] int id)
        {
            // if (User.Identity is not ClaimsIdentity identifier) return null;
            // if (identifier.FindFirst("Role").Value != "Teacher") return Forbid();

            return Ok(allTeachers.DeleteById(id));
        }
    }
}