﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using attendance_server.DbContexts;
using attendance_server.Interfaces;
using attendance_server.Mocks;
using attendance_server.Models;
using attendance_server.Models.Controllers;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace attendance_server.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Produces("application/json")]
    public class UsersController : ControllerBase
    {
        private readonly IAllUsers allUsers;
        private readonly IMapper mapper;

        public UsersController(IAllUsers allUsers, IMapper mapper)
        {
            this.allUsers = allUsers;
            this.mapper = mapper;
        }

        [HttpGet]
        public IEnumerable<User> List()
        {
            return allUsers.GetAll();
        }

        [HttpGet]
        [Route("{id}")]
        public ActionResult<User> Get(int id)
        {
            var user = allUsers.GetById(id);
            if (user == null)
                return NotFound();
            return user;
        }

        [HttpGet]
        [Route("current")]
        [Authorize]
        public ActionResult<User> GetCurrentUser()
        {
            if (User.Identity is not ClaimsIdentity identifier)
                return NotFound();
            var userId = identifier.FindFirst("Id")?.Value;
            if (userId == null)
                return NotFound();
            return Get(int.Parse(userId)).Value;
        }

        [HttpPost]
        [Route("add")]
        // [Authorize]
        public async Task<ActionResult<User>> Post([FromBody] AddUser body)
        {
            // if (User.Identity is not ClaimsIdentity identifier) return null;
            // if (identifier.FindFirst("Role").Value != "Teacher") return Forbid();
            var user = mapper.Map<User>(body);
            // Console.WriteLine($"Post {user}");
            allUsers.Add(user);
            return Ok(user);
        }


        [HttpDelete]
        [Route("{id}")]
        // [Authorize]
        public async Task<ActionResult<int>> DeleteUser(int id)
        {
            // if (User.Identity is not ClaimsIdentity identifier) return null;
            // if (identifier.FindFirst("Role").Value != "Teacher") return Forbid();

            Console.WriteLine($"Delete by id {id}");
            return Ok(allUsers.DeleteById(id));
        }
    }
}