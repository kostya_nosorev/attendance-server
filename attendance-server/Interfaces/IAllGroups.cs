﻿using System.Collections.Generic;
using attendance_server.Models;

namespace attendance_server.Interfaces
{
    public interface IAllGroups: IAllItems<Group>
    {
        Group GetGroupByName(string name);
        bool AddStudentsToGroupByStudentId(int groupId, IEnumerable<int> studentIds);
    }
}