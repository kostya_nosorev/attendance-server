using System.Collections.Generic;

namespace attendance_server.Interfaces
{
    public interface IAllItems<T>
    {
        List<T> GetAll();
        T GetById(int id);
        bool Add(T group);
        bool DeleteById(int id);
    }
}