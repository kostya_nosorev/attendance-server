﻿using System.Collections.Generic;
using attendance_server.Models;

namespace attendance_server.Interfaces
{
    public interface IAllUsers: IAllItems<User>
    {
        User GetUserByLoginPassword(string login, string password);
    }
}