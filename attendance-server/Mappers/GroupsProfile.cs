using attendance_server.Models;
using attendance_server.Models.Controllers;
using attendance_server.Views;
using AutoMapper;

namespace attendance_server.Mappers
{
    public class GroupsProfile: Profile
    {
        public GroupsProfile()
        {
            CreateMap<Student, SimpleStudentViewModel>();
            CreateMap<Group, GroupsViewModel>();
            CreateMap<User, UserViewModel>();
            CreateMap<AddGroup, Group>();
        }
    }
}