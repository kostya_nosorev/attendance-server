using attendance_server.Models;
using attendance_server.Models.Controllers;
using attendance_server.Views;
using AutoMapper;

namespace attendance_server.Mappers
{
    public class StudentsProfile: Profile
    {
        public StudentsProfile()
        {
            CreateMap<Student, StudentsViewModel>();
            CreateMap<Group, ListGroupViewModel>();
            CreateMap<User, UserViewModel>();
            CreateMap<AddStudent, Student>();
        }
    }
}