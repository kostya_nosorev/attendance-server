using attendance_server.Models;
using attendance_server.Models.Controllers;
using attendance_server.Views;
using AutoMapper;

namespace attendance_server.Mappers
{
    public class TeachersProfile: Profile
    
    {
        public TeachersProfile()
        {
            CreateMap<Teacher, TeachersViewModel>();
            CreateMap<User, UserViewModel>();
            CreateMap<AddTeacher, Teacher>();
        }
    }
}