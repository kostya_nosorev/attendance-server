using attendance_server.Models;
using attendance_server.Models.Controllers;
using AutoMapper;

namespace attendance_server.Mappers
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<AddUser, User>();
        }
    }
}