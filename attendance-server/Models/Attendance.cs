using System;
using attendance_server.Infrastructure;

namespace attendance_server.Models
{
    public class Attendance: Entity<int>
    {
        public int StudentId { get; private set; }
        public virtual Student Student { get; private set; }
        public int SubjectId { get; private set;}
        public virtual Subject Subject { get; private set;}
        public DateTime Date { get; private set; } 
        
        public Attendance(int id) : base(id)
        {
        }
    }
}