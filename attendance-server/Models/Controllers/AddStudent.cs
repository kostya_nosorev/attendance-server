using System;

namespace attendance_server.Models.Controllers
{
    public class AddStudent
    {
        public int userId { get; set; }
        public DateTime BirthDate { get; set; }
        public string UUID { get; set; }
    }
}