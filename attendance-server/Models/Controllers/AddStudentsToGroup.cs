using System.Collections.Generic;

namespace attendance_server.Models.Controllers
{
    public class AddStudentsToGroup
    {
        public int GroupId { get; set; }
        public List<int> StudentsIds { get; set; }
    }
}