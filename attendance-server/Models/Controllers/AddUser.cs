using attendance_server.Models.Enums;

namespace attendance_server.Models.Controllers
{
    public class AddUser
    {
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public UserTypeEnum UserType { get; set; }

        public string Login { get; set; }
        public string Password { get; set; }
    }
}