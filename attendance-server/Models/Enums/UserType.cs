namespace attendance_server.Models.Enums
{
    public enum UserTypeEnum
    {
        Student = 0,
        Teacher = 1
    }
}