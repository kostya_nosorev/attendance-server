using System.Collections.Generic;
using attendance_server.Infrastructure;
using attendance_server.Models.Controllers;

namespace attendance_server.Models
{
    public class Group : Entity<int>
    {
        public string Name { get; set; }
        public List<Student> Students { get; set; }

        public Group(int id) : base(id)
        {
        }

        public Group()
        {
        }

        public Group(int id, string name) : base(id)
        {
            Name = name;
        }
    }
}