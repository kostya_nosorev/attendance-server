using System;
using System.Collections.Generic;
using attendance_server.Infrastructure;
using attendance_server.Interfaces;
using attendance_server.Models.Controllers;

namespace attendance_server.Models
{
    public class Student : Entity<int>
    {
        public int UserId { get; set; }
        public User User { get; set; }
        public DateTime BirthDate { get; set; }
        public string UUID { get; set; }

        public List<Group> Groups { get; set; }
        public List<Attendance> Attendants { get; set; }

        public Student(int id, User user) : base(id)
        {
            UserId = user.Id;
        }

        public Student(int id, int userId, User user) : base(id)
        {
            UserId = userId;
            User = user;
        }

        public Student()
        {
        }

        public Student(int userId)
        {
        }
    }
}