using System.Collections.Generic;
using attendance_server.Infrastructure;

namespace attendance_server.Models
{
    public class Subject : Entity<int>
    {
        public string Name { get; private set; }
        public string Comment { get; private set; }
        public int SubjectTypeId { get; private set; }
        public SubjectType SubjectType { get; private set; }
        public List<Teacher> Teachers { get; private set; }

        public Subject(int id) : base(id)
        {
        }
    }
}