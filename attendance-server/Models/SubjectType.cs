using System.Collections.Generic;
using attendance_server.Infrastructure;

namespace attendance_server.Models
{
    public class SubjectType : Entity<int>
    {
        public string Name { get; set; }
        public List<Subject> Subjects { get; private set; }

        public SubjectType()
        {
        }
    }
}