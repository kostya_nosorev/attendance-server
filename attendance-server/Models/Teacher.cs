using System.Collections.Generic;
using attendance_server.Infrastructure;

namespace attendance_server.Models
{
    public class Teacher : Entity<int>
    {
        public int UserId { get; set; }
        public User User { get; set; }

        public List<Subject> Subjects { get; set; }

        public Teacher(int id, User user) : base(id)
        {
            UserId = user.Id;
        }

        public Teacher()
        {
        }
    }
}