using attendance_server.Infrastructure;
using attendance_server.Models.Enums;

namespace attendance_server.Models
{
    public class User : Entity<int>
    {
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public UserTypeEnum UserType { get; set; }

        public string Login { get; set; }
        public string Password { get; set; }

        public User()
        {
        }

        public User(int id, string lastName, string firstName, string middleName, UserTypeEnum userType) : base(id)
        {
            LastName = lastName;
            FirstName = firstName;
            MiddleName = middleName;
            UserType = userType;
        }
    }
}