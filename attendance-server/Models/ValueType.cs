using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using attendance_server.Infrastructure;

namespace attendance_server.Models
{
    /// <summary>
    /// Базовый класс для всех Value типов.
    /// </summary>
    public class ValueType<T> : IEquatable<T> where T : ValueType<T>
    {
        private static readonly Type Type = typeof(T);

        private static readonly IEnumerable<PropertyInfo> Properties = Type
            .GetProperties(BindingFlags.Instance | BindingFlags.Public)
            .OrderBy(member => member.Name);

        public override bool Equals(object obj)
        {
            return obj != null && (ReferenceEquals(this, obj) || Equals(obj as T));
        }

        public bool Equals(T other)
        {
            if (other == null) return false;
            if (ReferenceEquals(this, other)) return true;
            return Properties.Select(member => Equals(member.GetValue(this), member.GetValue(other)))
                .All(x => x);
        }

        public override int GetHashCode()
        {
            var hash = 0;
            unchecked
            {
                hash = Properties.Where(item => item.GetValue(this) != null)
                    .Aggregate(hash, (current, item) => item.GetValue(this).GetHashCode() ^ (current * 397));
            }

            return hash;
        }

        public override string ToString()
        {
            var items = Properties
                .Select(member => $"{member.Name}: {member.GetValue(this)}");
            return $"{Type.Name}({string.Join("; ", items)})";
        }
    }
}