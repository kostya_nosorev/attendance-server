﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using attendance_server.DbContexts;
using attendance_server.Interfaces;
using attendance_server.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace attendance_server.Mocks
{
    public class GroupsRepository : IAllGroups
    {
        public List<Group> GetAll()
        {
            using var context = new DBContext();
            var groups = context.Groups.Include(g => g.Students)
                .ThenInclude(s => s.User).ToList();
            return groups;
        }

        public Group GetById(int id)
        {
            using var context = new DBContext();
            var group = context.Groups.Include(g => g.Students)
                .ThenInclude(s => s.User)
                .FirstOrDefault(x => x.Id == id);
            return group;
        }

        public Group GetGroupByName(string name)
        {
            using var context = new DBContext();
            var group = context.Groups.Include(g => g.Students)
                .ThenInclude(s => s.User)
                .FirstOrDefault(x => x.Name == name);
            return group;
        }

        public bool Add(Group group)
        {
            using var context = new DBContext();
            context.Groups.Add(group);
            return context.SaveChanges() > 0;
        }

        public bool AddStudentsToGroupByStudentId(int groupId, IEnumerable<int> studentIds)
        {
            using var context = new DBContext();
            var group = context.Groups.FirstOrDefault(x => x.Id == groupId);
            if (group == null)
            {
                return false;
            }

            foreach (var studentId in studentIds)
            {
                var student = context.Students.First(x => x.Id == studentId);
                student.Groups.Add(group);
            }

            return context.SaveChanges() > 0;
        }

        public bool DeleteById(int id)
        {
            throw new NotImplementedException();
        }
    }
}