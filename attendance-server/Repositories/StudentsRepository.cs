﻿using System.Collections.Generic;
using System.Linq;
using attendance_server.DbContexts;
using attendance_server.Interfaces;
using attendance_server.Models;
using Microsoft.EntityFrameworkCore;

namespace attendance_server.Mocks
{
    public class StudentsRepository : IAllItems<Student>
    {
        public List<Student> GetAll()
        {
            using var context = new DBContext();
            var students = context.Students.Include(s => s.User)
                .Include(s => s.Groups).ToList();
            return students;
        }

        public Student GetById(int id)
        {
            using var context = new DBContext();
            var student = context.Students.Include(s => s.User)
                .Include(s => s.Groups).FirstOrDefault(x => x.Id == id);
            return student;
        }

        public bool Add(Student student)
        {
            using var context = new DBContext();
            context.Students.Add(student);
            var status = context.SaveChanges() > 0;
            if (status)
            {
                student.User = context.Users.First(x => x.Id == student.UserId);
            }

            return status;
        }

        public bool DeleteById(int id)
        {
            using var context = new DBContext();
            context.Students.Remove(context.Students.Single(x => x.Id == id));
            return context.SaveChanges() > 0;
        }
    }
}