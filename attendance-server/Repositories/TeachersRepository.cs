﻿using System.Collections.Generic;
using System.Linq;
using attendance_server.DbContexts;
using attendance_server.Interfaces;
using attendance_server.Models;
using Microsoft.EntityFrameworkCore;

namespace attendance_server.Mocks
{
    public class TeachersRepository : IAllItems<Teacher>
    {
        public List<Teacher> GetAll()
        {
            using var context = new DBContext();
            var teachers = context.Teachers
                .Include(s => s.User)
                .ToList();
            return teachers;
        }

        public Teacher GetById(int id)
        {
            using var context = new DBContext();
            var teacher = context.Teachers.Include(s => s.User)
                .FirstOrDefault(x => x.Id == id);
            return teacher;
        }

        public bool Add(Teacher teacher)
        {
            using var context = new DBContext();
            context.Teachers.Add(teacher);
            var status = context.SaveChanges() > 0;
            if (status)
            {
                teacher.User = context.Users.First(x => x.Id == teacher.UserId);
            }

            return status;
        }

        public bool DeleteById(int id)
        {
            using var context = new DBContext();
            context.Teachers.Remove(context.Teachers.Single(x => x.Id == id));
            return context.SaveChanges() > 0;
        }
    }
}