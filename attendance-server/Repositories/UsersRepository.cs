﻿using System.Collections.Generic;
using System.Linq;
using attendance_server.DbContexts;
using attendance_server.Interfaces;
using attendance_server.Models;

namespace attendance_server.Mocks
{
    public class UsersRepository : IAllUsers
    {
        public List<User> GetAll()
        {
            using var context = new DBContext();
            var users = context.Users.ToList();
            return users;
        }

        public User GetById(int id)
        {
            // return Users.FirstOrDefault(user => user.Id == id);
            using var context = new DBContext();
            var user = context.Users.FirstOrDefault(x => x.Id == id);
            return user;
        }

        public bool Add(User user)
        {
            using var context = new DBContext();
            context.Users.Add(user);
            return context.SaveChanges() > 0;
        }

        public bool DeleteById(int id)
        {
            using var context = new DBContext();
            context.Users.Remove(context.Users.Single(x => x.Id == id));
            return context.SaveChanges() > 0;
        }

        public User GetUserByLoginPassword(string login, string password)
        {
            using var context = new DBContext();
            var user = context.Users.FirstOrDefault(x => x.Login == login && x.Password == password);
            return user;
        }
    }
}