using System.Collections.Generic;
using attendance_server.Infrastructure;


namespace attendance_server.Views
{
    public class GroupsViewModel: Entity<int>
    {
        public string Name {get; set;}
        public List<SimpleStudentViewModel> Students { get; set; }
    }
}