using System;
using attendance_server.Infrastructure;

namespace attendance_server.Views
{
    public class SimpleStudentViewModel: Entity<int>
    {
        public UserViewModel User { get; set; }
        public DateTime BirthDate { get; set; }
        public string UUID { get; set; }
    }
}