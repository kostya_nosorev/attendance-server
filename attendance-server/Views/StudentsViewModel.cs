using System;
using System.Collections.Generic;
using attendance_server.Infrastructure;


namespace attendance_server.Views
{
    public class StudentsViewModel : Entity<int>
    {
        public UserViewModel User { get; set; }
        public DateTime BirthDate { get; set; }
        public string UUID { get; set; }

        public List<ListGroupViewModel> Groups { get; set; }
    }
}