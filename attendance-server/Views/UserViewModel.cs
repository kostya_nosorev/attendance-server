using attendance_server.Infrastructure;
using attendance_server.Models;
using attendance_server.Models.Enums;

namespace attendance_server.Views
{
    public class UserViewModel: Entity<int>
    {
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public UserTypeEnum UserType { get; set; }
    }
}